"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

from . import constants
from time import time
import requests

def javatime(stamp = None):
    if stamp is None:
        stamp = time()
    return int(stamp * 1000)

class XHR(object):
    HEADERS = {
        'Origin': constants.PORTAL_BASE,
        'Sec-Fetch-Site': 'same-site',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
    }

    @staticmethod
    def update_request_args(args):
        if 'headers' in args:
            headers = XHR.HEADERS
            headers.update(args['headers'])
            args['headers'] = headers
        else:
            args['headers'] = XHR.HEADERS

    @staticmethod
    def get(url, **kwargs):
        XHR.update_request_args(kwargs)
        req = requests.get(url, **kwargs)
        req.raise_for_status()
        return req.json()

    @staticmethod
    def post(url, **kwargs):
        XHR.update_request_args(kwargs)
        req = requests.post(url, **kwargs)
        req.raise_for_status()
        return req.json()
