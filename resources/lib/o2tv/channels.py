"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

from . import constants

class Channels(dict):
    def __init__(self, session, tariff=None, offers=None):
        super().__init__()

        self.session = session

        if tariff is None:
            tariff = self.session.subscription['billingParams']['tariff']
        self.tariff = tariff

        if offers is None:
            offers = self.session.subscription['billingParams']['offers']
        self.offers = offers

        if self.session.auth_type == 'IPTV':
            protocol = 'IGMP'
        else:
            protocol = 'DASH'
        self.protocol = protocol

        for offer in self.offers:
            params = {
                'isp': self.session.config['isp_id'],
                'locality': self.session.subscription['locality'],
                'tariff': self.tariff,
                'offer': offer,
                'deviceType': constants.DEVICE_TYPE,
                'liveTvStreamingProtocol': self.protocol,
            }
            data = self.session.get('server/tv/channels.json', params=params)
            self.update(data['channels'])
