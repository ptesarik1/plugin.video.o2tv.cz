"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

class EPG(dict):
    def __init__(self, session, epgid):
        super().__init__()

        self.session = session
        self.id = epgid

        params = {
            'epgId': self.id,
        }
        data = self.session.get('server/tv/epg-detail.json',
                                params=params)
        self.update(data)
