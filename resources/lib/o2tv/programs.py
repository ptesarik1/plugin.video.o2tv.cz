"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

from time import time

from .utils import javatime

class Programs(list):
    def __init__(self, session, channel, start=None, stop=None):
        super().__init__()

        self.session = session
        self.channel = channel

        now = int(time())
        if start is None:
            start = javatime(now - 7 * 24 * 60 * 60)
        self.start = start

        if stop is None:
            stop = javatime(now)
        self.stop = stop

        params = {
            'channelKey': self.channel,
            'fromTimestamp': self.start,
            'toTimestamp': self.stop,
        }
        data = self.session.get('server/tv/channel-programs.json',
                                params=params)
        self.extend(data)
