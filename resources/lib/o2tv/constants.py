"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

PORTAL_BASE = 'https://portal-ng.o2tv.cz'
PROFILE_CONF = '/prod_fa3/assets/configs/profile.conf'
MEDIATOR_BASE = 'http://ottmediator.o2tv.cz'
DEVICE_TYPE = 'STB'
DEVICE_NAME = 'STB#Kodi'
