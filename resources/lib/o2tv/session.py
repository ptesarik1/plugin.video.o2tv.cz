"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

import requests
from . import constants
from .utils import javatime, XHR

class Session(object):
    def __init__(self, device_id, language):
        self.device_id = device_id
        self.language = language

        params = { 'time': javatime() }
        req = requests.get(constants.PORTAL_BASE + constants.PROFILE_CONF,
                           params=params)
        req.raise_for_status()
        data = req.json()
        self.config = data['sections'][data['use_section']]

        params = { 'q': javatime() }
        data = XHR.get(self.config['authenticationType'], params=params)
        self.auth_type = data['authenticationType']
        if self.auth_type == 'OTT':
            self._authority = self.config['authority']
            self._login_url = self.config['login_url']
            self._get_access_token = self._get_access_token_ott
        else:
            self._authority = self.config['authority_iptv_mode']
            self._login_url = self.config['login_url_iptv_mode']
            self._get_access_token = self._get_access_token_iptv

    def _get_access_token_ott(self, username, password):
        params = {
            'username': username,
            'password': password,
        }
        data = XHR.get(self._login_url, params=params, data=b'{}')
        token = data['remote_access_token']

        params = {
            'service_id': self.config['services'][0]['service_id'],
            'remote_access_token': self.remote_access_token,
        }
        data = XHR.get(self.config['login_service_selection_url'],
                       params=params, data=b'{}')
        return token

    def _get_access_token_iptv(self, *args):
        params = { 'macAddress': self.device_id }
        data = XHR.get(self._login_url, params=params)
        return data['remote_access_token']

    def login(self, username, password):
        remote_access_token = self._get_access_token(username, password)

        params = {
            'client_id': self.config['oauth_client_id'],
            'client_secret': self.config['oauth_client_secret'],
            'grant_type': 'remote_access_token',
            'platform_id': self.config['oauth_platform_id'],
            'isp_id': self.config['isp_id'],
            'remote_access_token': remote_access_token,
            'authority': self._authority,
        }
        data = XHR.post(self.config['oauth'], params=params, data=b'{}')
        self.access_token = data['access_token']
        self.refresh_token = data['refresh_token']
        self.headers = {
            'X-NanguTv-Device-Id': self.device_id,
            'X-NanguTv-Device-Name': constants.DEVICE_NAME,
            'X-NanguTv-Access-Token': self.access_token,
        }

        self.subscription = self.get('subscription/settings/subscription-configuration.json')

    def update_request_args(self, args):
        params = { 'language': self.language }
        params.update(args.get('params', {}))
        args['params'] = params
        headers = self.headers
        headers.update(args.get('headers', {}))
        args['headers'] = headers

    def app_base(self):
        return self.config['app'] + 'sws/'

    def get(self, path, **kwargs):
        self.update_request_args(kwargs)
        return XHR.get(self.app_base() + path, **kwargs)

    def post(self, path, **kwargs):
        self.update_request_args(kwargs)
        return XHR.post(self.app_base() + path, **kwargs)
