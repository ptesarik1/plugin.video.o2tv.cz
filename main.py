"""

    Copyright (C) 2021 Petr Tesařík

    SPDX-License-Identifier: GPL-3.0-or-later
    See LICENSE.txt for more information.
"""

import sys
from urllib.parse import urlencode, parse_qs, quote
import time
import hashlib
import uuid

import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon

from resources.lib import o2tv

class Plugin(object):
    def __init__(self):
        # Get the plugin url in plugin:// notation.
        self.base_url = sys.argv[0]

        # Get the plugin handle as an integer number.
        self.handle = int(sys.argv[1])

        # Parse plugin parameters.
        self.params = dict()
        if len(sys.argv) >= 3:
            self.params.update(parse_qs(sys.argv[2][1:]))

        self.addon = xbmcaddon.Addon('plugin.video.o2tv.cz')

        # Read configuration
        username = self.addon.getSetting('username')
        password = self.addon.getSetting('password')
        language = self.addon.getSetting('language')
        device_id = self.addon.getSetting('device_id')
        if len(device_id) == 0:
            node = "%012x" % uuid.getnode()
            device_id = ':'.join([node[i:i+2] for i in range(0, 12, 2)])
            self.addon.setSetting('device_id', device_id)

        # Log in
        self.session = o2tv.Session(device_id, language)
        self.session.login(username, password)

    def get_channels(self):
        xbmcplugin.setPluginCategory(self.handle, 'O2TV Channels')
        xbmcplugin.setContent(self.handle, 'videos')
        tariff = self.addon.getSetting('tariff')
        if not tariff:
            return o2tv.Channels(self.session)
        return o2tv.Channels(self.session, tariff=tariff, offers=(tariff,))

    def list_channels(self):
        channels = self.get_channels()
        for ch in channels.values():
            if not ch.get('npvrEnabled', False):
                continue

            item = xbmcgui.ListItem(label=ch['channelName'])

            art = dict()
            logo = ch.get('logo')
            if logo is not None:
                # Hack the URL to get the full-size logo
                parts = logo.rsplit('/',5)
                del parts[1:-1]
                art['icon'] = '/'.join(parts)
            item.setArt(art)

            params = {
                'level': 'channel',
                'ch': ch['channelKey'],
            }
            target = self.base_url + '?' + urlencode(params)
            xbmcplugin.addDirectoryItem(self.handle, target, item, True)

        xbmcplugin.addSortMethod(
            self.handle,
            xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        xbmcplugin.endOfDirectory(self.handle)

    def list_epgs(self, channel):
        xbmcplugin.setPluginCategory(self.handle, channel)
        xbmcplugin.setContent(self.handle, 'videos')
        for epg in o2tv.Programs(self.session, channel):
            if not epg.get('npvrEnabled', False):
                continue

            item = xbmcgui.ListItem(label=epg['name'])
            item.setProperty('IsPlayable', 'true')

            start = epg['startTimestamp'] // 1000
            end = epg['endTimestamp'] // 1000
            info = {
                'dateadded': time.strftime('%Y-%m-%d %H:%M:%S',
                                           time.localtime(start)),
                'duration': end - start,
                'mediatype': 'video',
                'title': epg.get('name'),
                'plot': epg.get('shortDescription'),
            }
            item.setInfo('video', info)

            art = dict()
            landscape = epg.get('landscape')
            if landscape is not None:
                art['landscape'] = landscape
            picture = epg.get('picture')
            if picture is not None:
                art['thumb'] = picture
            item.setArt(art)

            params = {
                'level': 'epg',
                'epg': epg['epgId'],
            }
            target = self.base_url + '?' + urlencode(params)
            xbmcplugin.addDirectoryItem(self.handle, target, item, False)

        for meth in (xbmcplugin.SORT_METHOD_DATEADDED,
                     xbmcplugin.SORT_METHOD_DURATION,
                     xbmcplugin.SORT_METHOD_LABEL):
            xbmcplugin.addSortMethod(self.handle, meth)
        xbmcplugin.endOfDirectory(self.handle)

    # Get stream URL based on parameters
    def get_stream(self, service, obj, params):
        secret = self.session.config['streamer_shared_secret']
        elems = [ 'at', None, secret, 'subscr' ]
        elems.append(self.session.subscription['subscription'])
        elems.append(quote(self.session.device_id))
        if len(params) == 0:
            fname = '-'.join(obj)
        else:
            fname = '_'.join(('-'.join(obj), '-'.join(params)))
        elems.append(fname + '.mpd')

        now = o2tv.javatime()
        text = '/'.join(elems[2:]) + '{:08x}'.format(int(now))
        elems[1] = hashlib.md5(text.encode()).hexdigest()
        elems[2] = str(now)
        return self.session.config['stream_domain'] + '/'.join(elems)

    def play(self, epgid):
        epg = o2tv.EPG(self.session, epgid)
        channel = self.get_channels()[epg['channelKey']]

        # is this channel SD or HD?
        quality = 'sd'
        for src in channel['liveTvSources']:
            if src['resolution'] == 'HD':
                quality = 'hd'
                break
        if self.session.auth_type == 'OTT':
            quality = quality + '_ott'
        params = [ quality ]

        # These timestamps are in UTC
        now = time.time()

        start = epg['startTimestamp'] - channel['programStartOverlapTime']
        start //= 1000
        params.append(time.strftime('%Y%m%dT%H%M%S', time.gmtime(start)))

        end = epg['endTimestamp'] + channel['programEndOverlapTime']
        end //= 1000
        if end < now:
            params.append(time.strftime('%Y%m%dT%H%M%S', time.gmtime(end)))

        obj = (str(channel['chunkStoreChannelId']), 'tv', 'stb')
        url = self.get_stream('TIMESHIFT_TV', obj, params)

        item = xbmcgui.ListItem(path=url)
        item.setProperties({
            'inputstream': 'inputstream.adaptive',
            'inputstream.adaptive.manifest_type': 'mpd',
        })
        xbmcplugin.setResolvedUrl(self.handle, True, listitem=item)

        if end >= now:
            while not xbmc.Player().isPlayingVideo():
                xbmc.Monitor().waitForAbort(0.25)

            if xbmc.Player().isPlayingVideo():
                seek = start - now
                xbmc.executebuiltin('PlayerControl(SeekPercentage(0))')

    def run(self):
        level = self.params.get('level', (None,))[0]
        if level is None:
            self.list_channels()
        elif (level == 'channel'):
            self.list_epgs(self.params['ch'][0])
        elif (level == 'epg'):
            self.play(self.params['epg'][0])
        else:
            raise ValueError(f'Invalid parameters: {self.params}!')

if __name__ == '__main__':
    plugin = Plugin()
    plugin.run()
